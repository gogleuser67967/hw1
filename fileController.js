const fs = require("fs").promises;
const path = require('path');
const filesDirectory = '/files/';

function getFile(req, res){
  let fileData, fileStats;
  const { filename } = req.params;

  const readContent = () => {
    return fs.readFile(__dirname+filesDirectory+filename, "utf8")
      .then(result => {
        fileData = result;
      })
  }
  const setInfo = () => {
    return fs.stat(__dirname+filesDirectory+filename)
      .then(result => {
        fileStats = result;
      })
  }
  const sendResponse = () => {
    res.status(200).json({
      message: 'Success',
      filename: filename,
      content: fileData,
      extension: path.extname(filename).replace('.', ''),
      uploadedDate: fileStats?.birthtime
    });
  }
  const sendError = () => {
    res.status(500).json({
      "message": "Server error"
    })
  }
  readContent()
    .then(setInfo)
    .then(sendResponse)
    .catch(sendError)
}

function addFile(req, res){
  const { filename } = req.body;
  const { content } = req.body;

  fs.writeFile(__dirname+filesDirectory+filename, content)
    .then(() => res.status(200).json({message: "File created successfully"}))
    .catch(() => res.status(500).json({message: "Server error"}))
}

function getFiles(req, res){
  fs.readdir(__dirname+filesDirectory)
    .then(files => res.status(200).json({message: "Success", files}))
    .catch(() => res.status(500).json({message: "Server error"}))
}

async function deleteFile(req, res){
  const { filename } = req.params;

  fs.unlink(__dirname+filesDirectory+filename)
    .then(() => res.status(200).json({message: `File ${filename} was deleted`}))
    .catch(() => res.status(500).json({message: "Server error"}))
}

function updateFile(req, res){
  const { filename } = req.params;
  const { content } = req.body;

  fs.writeFile(__dirname+filesDirectory+filename, content)
    .then(() => res.status(200).json({message: `File ${filename} was updated`}))
    .catch(() => res.status(500).json({message: `Server error`}))
}

module.exports = {getFile, addFile, getFiles, deleteFile, updateFile}
