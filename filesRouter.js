const express = require('express');
// const {checkGetFile, checkAddFile, setPassword, checkAccess} = require("./middlewares");
const checkAddFile = require('./middlewares/checkAddFile');
const setPassword = require('./middlewares/setPassword');
const checkGetFile = require('./middlewares/checkGetFile');
const checkAccess = require('./middlewares/checkAccess');
const removePassword = require('./middlewares/removePassword');
const {addFile, getFile, getFiles, deleteFile, updateFile} = require("./fileController");
const router = express.Router();


router.get('/', getFiles);
router.post('/', checkAddFile, setPassword, addFile)
router.get('/:filename', checkGetFile, checkAccess, getFile);
router.delete('/:filename', checkGetFile, checkAccess, removePassword, deleteFile);
router.put('/:filename', checkGetFile, checkAccess, updateFile);

module.exports = router;
