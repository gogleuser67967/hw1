const express = require('express');
const morgan = require("morgan");
const router = require("./filesRouter");
const app = express();
const port = 8080;

app.use(morgan('combined'));
app.use(express.json());
app.set('json spaces', 2);

app.use('/api/files', router);
app.use("*", (req, res) => res.status(400).json({message: "Bad route"}));

app.listen(port, () => {
  console.log('Server started');
})
