const fs = require("fs").promises;

module.exports = function checkAccess(req, res, next){
  const { password } = req.query;
  const { filename } = req.params;

  fs.readFile(__dirname+'/../password.json', "utf8")
    .then((result) => {
      checkPass(JSON.parse(result))
    })
    .catch(() => res.status(500).json({"message": "Server error"}))

  function checkPass(passFile){
    let current = passFile.passwords.find(el => el[0] === filename);
    if(current && current[1] !== password){
      return res.status(400).json({"message": "Incorrect password"});
    }
    else{
      next();
    }
  }
}

