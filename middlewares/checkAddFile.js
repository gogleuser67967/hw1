const path = require("path");
const fs = require("fs").promises;
const extensions = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];

module.exports = async function checkAddFile(req, res, next){
  try{
    const { filename } = req.body;
    const { content } = req.body;
    if(typeof content != "string"){
      return res.status(400).json({message: "Please specify 'content' parameter"})
    }
    if(typeof filename != "string"){
      return res.status(400).json({message: "Please specify 'filename' parameter"})
    }
    if(!extensions.find(element => element === path.extname(filename))){
      return res.status(400).json({message: `This extension is not allowed`})
    }
    // try{
    //   await fs.access(`./files/${filename}`);
    //   return res.status(400).json({message: `This file already exists`});
    // }
    // catch (e){}
    next();
  }
  catch (error){
    return res.status(500).json({message: "Server error"})
  }
}
