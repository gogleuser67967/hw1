const fs = require("fs").promises;

module.exports = function checkGetFile(req, res, next){
  const { filename } = req.params;
  if(!filename){
    return res.status(400).json({
      "message": `No filename`
    })
  }
  fs.access(`./files/${filename}`)
    .then(() => {
      next();
    })
    .catch(() => res.status(400).json({
      "message": `No file with '${filename}' filename found`
    }))
}
