const fs = require("fs").promises;

module.exports = async function removePassword(req, res, next){
  const {filename} = req.params;

  try{
    let data = await fs.readFile(__dirname+'/../password.json');
    data = JSON.parse(data);
    data = {passwords: data.passwords.filter(el => el[0] !== filename)}
    await fs.writeFile(__dirname+'/../password.json', JSON.stringify(data, null, 2));
    next();
  }
  catch (e){
    res.status(500).json({message: "Server error"});
  }
}
