const fs = require("fs").promises;

module.exports = async function setPassword(req, res, next) {
  const {password} = req.body;
  const {filename} = req.body;

  if (password) {
    try {
      let result = await fs.readFile(__dirname + '/../password.json', "utf8");
      result = JSON.parse(result);
      let current = result.passwords.find(el => el[0] === filename);
      if (current) {
        current[1] = password;
      }
      else {
        result.passwords.push([filename, password]);
      }
      await fs.writeFile(__dirname + '/../password.json', JSON.stringify(result, null, 2));
      next();
    } catch (e) {
      res.status(500).json({message: "Server error"})
    }
  }
  else{
    next();
  }
}
